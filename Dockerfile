# specify a suitable source image
FROM node:latest

WORKDIR /app

COPY . ./

RUN npm install
RUN npm ci

# copy the application source code files

EXPOSE 3000

# specify the command which runs the application

CMD npm run start